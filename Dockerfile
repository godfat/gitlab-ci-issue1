FROM ruby:2.3-alpine

MAINTAINER Tobias L. Maier <tobias.maier@baucloud.com>

ENV PORT 5000
EXPOSE $PORT

# do not install documentation for ruby gems
RUN echo "gem: --no-document" >> /etc/gemrc

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY ["Gemfile", "Gemfile.lock", "/usr/src/app/"]

RUN bundle install
COPY . /usr/src/app

CMD sh
