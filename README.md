# Inconsistent behaviour between custom docker image and running it directly

See <https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/2014> for details on the issue described here.